#ifndef _GEMEVENT_
#define _GEMEVENT_
// TObject class to write a feGEM BANK to a ROOT tree
#include "GEM_BANK.h"
// ROOT headers:
#include "TObject.h"
// std lib
#include "assert.h"
/** \class TStoreGEMEventHeader
A class for storing feGEM databank header information in a root file

*/
class TStoreGEMEventHeader : public TObject {
private:
   // Maybe make all this const...
   std::string BANK;
   std::string DATATYPE;
   std::string VARCATEGORY;
   std::string VARNAME;
   std::string EquipmentType;
   uint16_t    HistorySettings;
   uint16_t    HistoryPeriod;
   uint16_t    TimestampEndianness;
   uint16_t    DataEndianness;
   uint32_t    BlockSize;
   uint32_t    NumberOfEntries;

public:
   TStoreGEMEventHeader();
   TStoreGEMEventHeader(GEMBANK<void *> *bank);
   virtual ~TStoreGEMEventHeader();
   ClassDef(TStoreGEMEventHeader, 1)
};

/** \class TLVTimestamp
 * \brief A class for storing the timestamp (LabVIEW format) in TStoreGEMData
 * 
 * The LabVIEW timestamp is a 128-bit of data, in Big Endian...

 * @code
 * {
 *    (i64) seconds since the epoch 01/01/1904 00:00:00.00 UTC (using the Gregorian calendar and ignoring leap seconds),
 *    (u64) positive fractions of a second
 * }
 * @endcode
 * https://www.ni.com/en-us/support/documentation/supplemental/08/labview-timestamp-overview.html
*/

class TLVTimestamp : public TObject {

public:
   // LabVIEW formatted time... (128bit) in Big Endian!  
   /// (i64) seconds since the epoch 01/01/1904 00:00:00.00 UTC (using the Gregorian calendar and ignoring leap seconds),
   int64_t Seconds;
   /// (u64) positive fractions of a second
   uint64_t SubSecondFraction;
public:
   TLVTimestamp();
   TLVTimestamp(bool Now );
   virtual ~TLVTimestamp();
   TLVTimestamp(const TLVTimestamp &ts);
   TLVTimestamp &operator=(const LVTimestamp &ts);
   TLVTimestamp &operator=(const TLVTimestamp &ts);
   ClassDef(TLVTimestamp, 1);
};



/** \class TStoreGEMData
 * \brief A class for storing the feGEM data in root
 * 
 * Contains a TLVTimestamp, and the data delivered in the databank
*/
template <class T>
class TStoreGEMData : public TObject {
private:
   // Variable order hasn't been memory layout optimised
   TLVTimestamp   RawLabVIEWtimestamp;
   double         RawLabVIEWAsUNIXTime;
   uint16_t       TimestampEndianness;
   uint16_t       DataEndianness;
   uint32_t       MIDASTime;
   int            runNumber;
   double         RunTime;
   std::vector<T> data;

public:
   TStoreGEMData();
   void                  Set(const GEMDATA<T> *gemdata, const int BlockSize, const uint16_t _TimestampEndianness,
                             const uint16_t _DataEndianness, const uint32_t _MIDASTime, const double _RunTime, const int runNumber);
   double                GetLVTimestamp() const { return RawLabVIEWAsUNIXTime; }
   double                GetRunTime() const { return RunTime; }
   int                   GetRunNumber() const { return runNumber; }
   T                     GetArrayEntry(int i) const { return data.at(i); }
   const std::vector<T> &GetData() const { return data; }
   virtual ~TStoreGEMData();
   ClassDef(TStoreGEMData<T>, 1);
};


/** \class TStoreGEMFile
 * \brief Specialisation of the TStoreGEMData for saving 'Files' sent from the LabVIEW client
 * 
 * Contains a TLVTimestamp, and the data delivered in the databank 
 * along with MD5 check sum, filename and file path from which the data was sent
*/
class TStoreGEMFile : public TStoreGEMData<char> {
private:
   std::string filename;
   std::string filepath;
   std::string MD5;

public:
   TStoreGEMFile();
   void SetFileName(const char *_filename, const char *_filepath, const char *_MD5)
   {
      filename = _filename;
      filepath = _filepath;
      MD5      = _MD5;
   }
   std::string GetFileName() const { return filename; }
   std::string GetFilePath() const { return filepath; }
   std::string GetFileMD5() const { return MD5; }
   void        PrintFileInfo() const
   {
      std::cout << "filename: " << filename << std::endl;
      std::cout << "filepath: " << filepath << std::endl;
      std::cout << "MD5:" << MD5 << std::endl;
   }
   virtual ~TStoreGEMFile();
   ClassDef(TStoreGEMFile, 1);
};

#endif
