#include "TStoreGEMEvent.h"

/// @brief Constructor
TStoreGEMEventHeader::TStoreGEMEventHeader()
{
   // ctor
}

/// @brief Constructor from GEMBANK (feGEM native network transported data)
/// @param bank
TStoreGEMEventHeader::TStoreGEMEventHeader(GEMBANK<void *> *bank)
{
   BANK                = bank->GetBANK();
   DATATYPE            = bank->GetType();
   VARCATEGORY         = bank->GetCategoryName();
   VARNAME             = bank->GetVariableName();
   EquipmentType       = bank->GetEquipmentType();
   HistorySettings     = bank->HistorySettings;
   HistoryPeriod       = bank->HistoryPeriod;
   TimestampEndianness = bank->TimestampEndianness;
   DataEndianness      = bank->DataEndianness;
}

/// @brief Deconstructor
TStoreGEMEventHeader::~TStoreGEMEventHeader()
{
   // dtor
}

/// @brief Default constructor
///
/// Initilise the timestamp to '0' ( 01/01/1904 00:00:00.00 UTC )
TLVTimestamp::TLVTimestamp()
{
   Seconds           = 0;
   SubSecondFraction = 0;
}

/// @brief Constructor
/// @param Now boolean to toggle constructor mode
///
/// If Now is set to true, a TLVTimestamp will be created using the system clock (std::chrono)
///
/// If Now is set to false, a TLVTimeStamp will be created but initialised to 0  ( 01/01/1904 00:00:00.00 UTC )
TLVTimestamp::TLVTimestamp(bool Now)
{
   if (!Now) {
      Seconds           = 0;
      SubSecondFraction = 0;
      return;
   }

   using namespace std::chrono;

   system_clock::time_point tp  = system_clock::now();
   system_clock::duration   dtn = tp.time_since_epoch();

   Seconds    = dtn.count() * system_clock::period::num / system_clock::period::den; //+2082844800;
   uint64_t f = dtn.count() - Seconds * system_clock::period::den / system_clock::period::num;
   f /= system_clock::period::den;
   double fraction   = (double)f;
   SubSecondFraction = fraction * (double)((uint64_t)-1);
   // Convert from UNIX time (seconds since 1970) to LabVIEW time (seconds since 01/01/1904 )
   Seconds += 2082844800;

   // LabVIEW timestamp is big endian... conform...
   Seconds           = change_endian(Seconds);
   SubSecondFraction = change_endian(SubSecondFraction);
   // print();
}
/// @brief Deconstructor
TLVTimestamp::~TLVTimestamp() {}
/// @brief Copy constuctor
/// @param ts
TLVTimestamp::TLVTimestamp(const TLVTimestamp &ts) : TObject(ts)
{
   Seconds           = ts.Seconds;
   SubSecondFraction = ts.SubSecondFraction;
}

/// @brief Equals operator from TLVTimestamp
/// @param ts
/// @return
TLVTimestamp &TLVTimestamp::operator=(const TLVTimestamp &ts)
{
   Seconds           = ts.Seconds;
   SubSecondFraction = ts.SubSecondFraction;
   return *this;
}
/// @brief Equals operator from LVTimeStamp (native feGEM data)
/// @param ts
/// @return
TLVTimestamp &TLVTimestamp::operator=(const LVTimestamp &ts)
{
   Seconds           = ts.Seconds;
   SubSecondFraction = ts.SubSecondFraction;
   return *this;
}

/// @brief Constructor
/// @tparam T type specialisation of class
template <class T>
TStoreGEMData<T>::TStoreGEMData()
{
}

/// @brief Set contents of the TStoreGEMData<T>
/// @tparam T type specialisation of class
/// @param gemdata feGEM databank
/// @param BlockSize
/// @param _TimestampEndianness LabVIEW timestamp Endianess in bank
/// @param _DataEndianness Endianess of data in bank
/// @param _MIDASTime Time according to MIDAS
/// @param RunTimeOffset Offset between run time and MIDAS time (calculated at the start of run during analysis)
/// @param _runNumber Current run number
template <class T>
void TStoreGEMData<T>::Set(const GEMDATA<T> *gemdata, const int BlockSize, const uint16_t _TimestampEndianness,
                           const uint16_t _DataEndianness, const uint32_t _MIDASTime, const double RunTimeOffset,
                           const int _runNumber)
{
   RawLabVIEWtimestamp = gemdata->timestamp;
   // Please check this calculation
   RawLabVIEWAsUNIXTime = gemdata->GetUnixTimestamp(_TimestampEndianness);
   // Really, check this fraction calculation
   double fraction = (double)gemdata->GetLabVIEWFineTime(_TimestampEndianness) / (double)((uint64_t)-1);
   assert(fraction < 1);
   RawLabVIEWAsUNIXTime += fraction;
   TimestampEndianness = _TimestampEndianness;
   DataEndianness      = _DataEndianness;
   MIDASTime           = _MIDASTime;
   // std::cout<<"MIDAS:"<< MIDASTime;
   runNumber = _runNumber;
   RunTime   = RawLabVIEWAsUNIXTime - RunTimeOffset;
   // std::cout<<"RunTime"<<RunTime<<std::endl;
   data.clear();
   int entries = gemdata->GetEntries(BlockSize);
   data.reserve(entries);
   for (int i = 0; i < entries; i++) {
      // std::cout<<"DATA: "<<gemdata->DATA[i];
      data.push_back(*gemdata->DATA(i));
      // std::cout<<"\t"<<data.back()<<std::endl;
   }
}

/// @brief Deconstructor
/// @tparam T type specialialisation of class
///
/// Precautionarily clean the std::vector<T> inside the class
template <class T>
TStoreGEMData<T>::~TStoreGEMData()
{
   data.clear();
}

/// @brief Constructor
///
/// Simple the constructor of parent TStoreGEMData<char>
TStoreGEMFile::TStoreGEMFile() : TStoreGEMData<char>() {}

/// @brief Deconstructor
TStoreGEMFile::~TStoreGEMFile() {}

// Define all valid data types for TStoreGEMData

template class TStoreGEMData<double>;
template class TStoreGEMData<float>;
template class TStoreGEMData<bool>;
template class TStoreGEMData<int32_t>;
template class TStoreGEMData<uint32_t>;
template class TStoreGEMData<uint16_t>;
template class TStoreGEMData<char>;
