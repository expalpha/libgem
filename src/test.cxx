#include "GEM_BANK.h"
template <int S, typename T>
class TEST_DOUBLE_DATA {
public:
   int64_t Seconds;
   //(u64) positive fractions of a second 
   uint64_t SubSecondFraction;
   // 8 bytes x S
   T Data[S];
   TEST_DOUBLE_DATA()
   {
      Seconds = 62167219200;
      SubSecondFraction = 0;
      for (int i = 0; i < S; i++)
         Data[i] = T(i);
   }
}__attribute__((packed));

template <int N, int S, typename T>
struct TEST_BANK {
   char BANK[4]={'T','E','S','T'}; //LVB1
   char DATATYPE[4]={'C','H','K','\0'}; //DBLE, UINT, INT6, INT3, INT1, INT8, CHAR
   char VARCATEGORY[16]="Category\0\0\0\0\0\0\0";
   char VARNAME[16]="Varname\0\0\0\0\0\0\0\0";
   char EquipmentType[32]="Equipment Type\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0";
   
   uint16_t HistorySettings = 1;
   uint16_t HistoryPeriod = 2;
   uint16_t TimestampEndianness = LittleEndian;
   uint16_t DataEndianness = LittleEndian;
   uint32_t BlockSize = sizeof(T)*S + 8 + 8;
   uint32_t NumberOfEntries = N;
   TEST_DOUBLE_DATA<S,T> DATA[N];
/*   int64_t Seconds = 62167219200;
   //(u64) positive fractions of a second 
   uint64_t SubSecondFraction = 0;
   // 8 bytes x 10
   double Data[10] = { 0., 1., 2., 3., 4., 5., 6., 7., 8., 9. };*/
} __attribute__((packed));


#include <assert.h>

template <int BANKS, int ARRAY_SIZE, typename T>
void RunBankTest()
{

   // TEST_BANK is a packed data structure (byte alignment is turned off.)
   TEST_BANK<BANKS,ARRAY_SIZE,T> a;
   // Use a temporary pointer to hide this warning:
   // warning: converting a packed ‘TEST_BANK<10, 1, char>’ pointer (alignment 1) to a ‘GEMBANK<char>’ pointer (alignment 4) may result in an unaligned pointer value [-Waddress-of-packed-member]

   void* c = (void*)&a;
   GEMBANK<T>* b = (GEMBANK<T>*)c;
   assert(strncmp(a.BANK,b->GetBANK().c_str(),4)==0 );

   assert(strncmp(a.DATATYPE, b->GetType().c_str(),4)==0);

   assert(b->GetCategoryName().size() > 0);
   assert(strncmp(a.VARCATEGORY, b->GetCategoryName().c_str(), b->GetCategoryName().size() )==0);

   assert(b->GetVariableName().size() > 0);
   assert(strncmp(a.VARNAME, b->GetVariableName().c_str(), b->GetVariableName().size() )==0);
   
   assert(b->GetEquipmentType().size() > 0);
   //This test fails! The space is sanitised away! (Hence the 'not' at begin of assert)
   assert( not strncmp(a.EquipmentType, b->GetEquipmentType().c_str(),b->GetEquipmentType().size() ) ==0 );
   //This test passed
   assert(strncmp("EquipmentType", b->GetEquipmentType().c_str(),b->GetEquipmentType().size() ) ==0 );

   //This is only testing one case... perhaps we should test multiple variable lengths
   std::string CombinedName = std::string(a.VARCATEGORY) + std::string("\\") + std::string(a.VARNAME);
   assert (CombinedName == b->GetCombinedName() );
   
   assert( a.HistorySettings == b->HistorySettings);
   assert(a.HistoryPeriod == b->HistoryPeriod);
   assert(a.TimestampEndianness == b->TimestampEndianness);
   assert(a.DataEndianness == b->DataEndianness);
   assert(a.BlockSize == b->BlockSize);
   assert(a.NumberOfEntries == b->NumberOfEntries);

   assert(a.DATA[0].Seconds == b->GetFirstDataEntry()->timestamp.Seconds);
   assert(a.DATA[0].SubSecondFraction == b->GetFirstDataEntry()->timestamp.SubSecondFraction);
   std::cout<<b->GetSizeOfDataArray()<<std::endl;
   assert(b->GetSizeOfDataArray() == ARRAY_SIZE);


   for (size_t i = 0 ; i < a.NumberOfEntries; i++)
   {
      assert(a.DATA[i].Seconds == b->GetDataEntry(i)->timestamp.Seconds);
      assert(a.DATA[i].SubSecondFraction == b->GetDataEntry(i)->timestamp.SubSecondFraction);
      for (size_t j = 0; j < b->GetSizeOfDataArray(); j++)
      {
         assert(a.DATA[i].Data[j] == *(b->GetDataEntry(i)->DATA(j)));
      }
   }

   assert(a.DATA[a.NumberOfEntries - 1].Seconds == b->GetLastDataEntry()->timestamp.Seconds);
   assert(a.DATA[a.NumberOfEntries - 1].SubSecondFraction == b->GetLastDataEntry()->timestamp.SubSecondFraction);
   
   
   assert(a.DATA[0].Seconds == b->GetFirstUnixTimestamp() + 2082844800);
   assert(a.DATA[a.NumberOfEntries - 1].Seconds == b->GetLastUnixTimestamp() + 2082844800);
   std::cout<<"Tests passed!"<<std::endl;
   return;
}


#include <type_traits>
#include <typeinfo>
#ifndef _MSC_VER
#   include <cxxabi.h>
#endif
#include <memory>
#include <string>
#include <cstdlib>

template <class T>
std::string
type_name()
{
    typedef typename std::remove_reference<T>::type TR;
    std::unique_ptr<char, void(*)(void*)> own
           (
#ifndef _MSC_VER
                abi::__cxa_demangle(typeid(TR).name(), nullptr,
                                           nullptr, nullptr),
#else
                nullptr,
#endif
                std::free
           );
    std::string r = own != nullptr ? own.get() : typeid(TR).name();
    if (std::is_const<TR>::value)
        r += " const";
    if (std::is_volatile<TR>::value)
        r += " volatile";
    if (std::is_lvalue_reference<T>::value)
        r += "&";
    else if (std::is_rvalue_reference<T>::value)
        r += "&&";
    return r;
}


template <typename T>
void RunManyBankTests()
{
   T i;
   std::cout<<"Testing "<< type_name<decltype(i)>() <<std::endl;
   RunBankTest<2,10,T>();
   RunBankTest<1,100,T>();
   RunBankTest<10,1,T>();
   RunBankTest<100,10000,T>();
}


int main()
{
   RunManyBankTests<double>();
   RunManyBankTests<char>();
   RunManyBankTests<int>();
   RunManyBankTests<bool>();
}
