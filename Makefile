INC_DIR   = $(MIDASSYS)/include
LIB_DIR   = $(MIDASSYS)/lib
SRC_DIR   = $(MIDASSYS)/src

CC     = gcc
CFLAGS = -Wall -Wextra -O2 -g -Wall -DOS_LINUX -Dextname
CFLAGS += -std=c++11 -Wall -O2 -g -Iinclude -IlibGEM/include -I$(INC_DIR) -I$(MIDASSYS)/mxml/ -I$(MIDASSYS)/mvodb/ -I$(MIDASSYS)/mjson/
CFLAGS += $(PGFLAGS)
CFLAGS += -I$(shell root-config --incdir) -fPIC
LIBS = -lm -lz -lutil -lnsl -lpthread 

INC= include/TStoreGEMEvent.h include/GEM_BANK.h
INC+=LibLinkDef.hh

DICT=GEMlib.cc

SRC=src/TStoreGEMEvent.cxx src/GEM_BANK.cxx $(DICT)
SRC:= $(patsubst src/%.cxx,%.o,$(SRC))
#MODBUS_DIR = $(MIDASSYS)/drivers/divers
#CFLAGS += -I$(MODBUS_DIR)
ROO=rootcint

MODULES = $(LIB_DIR)/mfe.o 

all::libGEM.so

libGEM.so: $(SRC) $(INC)
	$(CXX) -shared $(CFLAGS) $(LDFLAGS) $(LIBS) -lssl -lcrypto -o $@  $^

$(DICT): $(INC)
	$(ROO) -f $@ -c $(CFLAGS) $(INCS) -p $^

feGEMAnalyzer/%.o: feGEMAnalyzer/%.cxx
	$(CXX) -o $@ $(CFLAGS) -c $<

%.o: src/%.cxx
	$(CXX) -o $@ $(CFLAGS) -c $<

clean::
	-rm -f *.o GEMlib.cc GEMlib_rdict.pcm *~ \#* *.exe



#end
